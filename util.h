/* 
 * File:   util.h
 * Author: ilya
 *
 * Created on 31 Май 2012 г., 10:27
 */

#ifndef UTIL_H
#define	UTIL_H

#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
/*
#include <boost/random.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>
*/
#include <ctime>

using namespace std;
//using namespace boost;

void stoupper(std::string& s);
char* itoa(int value, char* result, int base);
string MakeSeqComplement(string init_str);
//int GetRandomInt (int from, int to);
//double GetRandomDouble (double from, double to);
void split_str(const string& str, vector<string>& tokens, const string& delimiters);
void TrimNs(string &read);
string GenNs(int num, char* letter);

#endif	/* UTIL_H */

