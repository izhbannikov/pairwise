CXX = g++
CFLAGS = -Wall -g 


all:  util.o pairwise.o main.o pairwise 
		
					
pairwise :   main.o pairwise.o util.o
	$(CXX) $(CFLAGS)  -o  pairwise main.o pairwise.o util.o -lpthread -Xlinker -zmuldefs
	
main.o :  
	$(CXX) $(CFLAGS)  -c main.cpp 

pairwise.o :  
	$(CXX) $(CFLAGS)  -c pairwise.cpp
	
util.o :
	$(CXX) $(CFLAGS)  -c util.cpp

clean:
	rm -rf pairwise *.o
