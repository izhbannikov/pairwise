#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include "timer.h"
#include "util.h"
#include <streambuf>
#include <exception>
#include "pairwise.h"

using namespace std;

int main(int argc, char *argv[]) {
    double start, finish, elapsed;
    
    GET_TIME(start);	
    
    string ref_string = "AACTACTATACGAGTTGAT";
    string query_string = "GACTCCTATACGAGTGGAT";
    
    banded(ref_string, query_string, 2, 1);
    
    GET_TIME(finish);
    elapsed = finish - start;
    printf("Elapsed time = %e seconds\n", elapsed);
    
    start = finish = elapsed = 0;    
        
    return 0;
}



